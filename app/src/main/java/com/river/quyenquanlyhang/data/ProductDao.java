package com.river.quyenquanlyhang.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.river.quyenquanlyhang.Product;

import java.util.ArrayList;

public class ProductDao {

    SQLiteDatabase db;
    Database dtb;

    public ProductDao(Context context) {
        Database database = new Database(context);
        this.dtb = database;
        this.db = database.getWritableDatabase();
    }

    public ArrayList<Product> getAll() {
        ArrayList<Product> list = new ArrayList<>();
        Cursor cs = this.db.rawQuery("SELECT * FROM product", null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            list.add(new Product(cs.getString(0), cs.getString(1), cs.getString(2), cs.getInt(3), cs.getInt(4)));
            cs.moveToNext();
        }
        cs.close();
        return list;
    }

//    loai text, ma text, ten text, soluong int, gia
    public boolean them(Product p) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("loai", p.getLoai());
        contentValues.put("ma", p.getMa());
        contentValues.put("ten", p.getTen());
        contentValues.put("soluong", p.getSoLuong());
        contentValues.put("gia", p.getDonGia());
        if (this.db.insert("product", null, contentValues) <= 0) {
            return false;
        }
        return true;
    }

    public boolean sua(Product p) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("loai", p.getLoai());
        contentValues.put("ten", p.getTen());
        contentValues.put("soluong", p.getSoLuong());
        contentValues.put("gia", p.getDonGia());
        return this.db.update("product", contentValues, "ma=?", new String[]{p.getMa()}) > 0;
    }

    public boolean xoa(String ma) {
        String str = "ma=?";
        int r = this.db.delete("product", str, new String[]{ma});
        return r > 0;
    }
}
