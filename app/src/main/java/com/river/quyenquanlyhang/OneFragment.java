package com.river.quyenquanlyhang;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.river.quyenquanlyhang.data.ProductDao;

import java.util.ArrayList;
import java.util.List;


public class OneFragment extends Fragment {

    List<Product> list = new ArrayList<Product>();
    ProductAdapter productAdapter;
    ProductDao productDao;
    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_one, container, false);

        productDao = new ProductDao(getContext());

        List<Product> image_details = productDao.getAll();
        listView = root.findViewById(R.id.listView);
        productAdapter = new ProductAdapter(getContext(), image_details);
        listView.setAdapter(productAdapter);


        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView btnAdd = view.findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                ViewGroup viewGroup = view.findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.input_product, viewGroup, false);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();

                final EditText etLoai = dialogView.findViewById(R.id.etLoai);
                final EditText etMa = dialogView.findViewById(R.id.etMa);
                final EditText etTen = dialogView.findViewById(R.id.etTen);
                final EditText etSoLuong = dialogView.findViewById(R.id.etSoLuong);
                final EditText etGia = dialogView.findViewById(R.id.etGia);

                Button btnLuu = dialogView.findViewById(R.id.btnLuu);
                Button btnHuy = dialogView.findViewById(R.id.btnHuy);

                btnLuu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Product product = new Product(etLoai.getText().toString(),
                                etMa.getText().toString(),
                                etTen.getText().toString() ,
                                Integer.parseInt(etSoLuong.getText().toString()) ,
                                Integer.parseInt(etGia.getText().toString()) );

                        if(productDao.them(product)){
                            productAdapter.submitList(productDao.getAll());
                            Toast.makeText(getContext(), "Luu thanh cong!", Toast.LENGTH_LONG).show();
                        } else{

                            Toast.makeText(getContext(), "Luu that bai!", Toast.LENGTH_LONG).show();
                        }
                        alertDialog.dismiss();
                    }
                });

                btnHuy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

            }
        });
    }

    private List<Product> getListData() {

        Product product = new Product("Ao", "AO01", "Áo thun", 100, 100000);
        list.add(product);

        for (int i = 0; i < 100; i++) {
            product = new Product("Quan", "Quan01", "Quan thun", 20, 100000);
            list.add(product);
        }
        return list;
    }
}