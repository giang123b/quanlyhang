package com.river.quyenquanlyhang.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {
    public Database(Context context) {
        super(context, "Quan li", null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE product(loai text, ma text, ten text, soluong int, gia int)");
    }

    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
    }
}
