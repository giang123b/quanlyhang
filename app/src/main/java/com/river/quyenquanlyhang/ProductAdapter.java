package com.river.quyenquanlyhang;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.river.quyenquanlyhang.data.ProductDao;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends BaseAdapter {

    private List<Product> listData;
    private LayoutInflater layoutInflater;
    private Context context;

    public ProductAdapter(Context aContext, List<Product> listData) {
        this.context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_product, null);
            holder = new ViewHolder();

            holder.loai =  convertView.findViewById(R.id.tvLoai);
            holder.ma =  convertView.findViewById(R.id.tvMa);
            holder.ten =  convertView.findViewById(R.id.tvTen);
            holder.soLuong =  convertView.findViewById(R.id.tvGia);
            holder.donGia =  convertView.findViewById(R.id.tvGia);
            holder.ivXoa =  convertView.findViewById(R.id.ivXoa);
            holder.ivSua =  convertView.findViewById(R.id.ivSua);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Product product = this.listData.get(position);

        holder.loai.setText(product.getLoai());
        holder.ma.setText(product.getMa());
        holder.ten.setText(product.getTen());
        holder.soLuong.setText( "" + product.getSoLuong());
        holder.donGia.setText( "" +product.getDonGia() + " VND");

        final View finalConvertView = convertView;
        holder.ivXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductDao productDao = new ProductDao(finalConvertView.getContext());
                productDao.xoa(product.getMa());
                submitList(productDao.getAll());
            }
        });

        holder.ivSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(finalConvertView.getContext());
                ViewGroup viewGroup = view.findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.input_product, viewGroup, false);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();

                final EditText etLoai = dialogView.findViewById(R.id.etLoai);
                final EditText etMa = dialogView.findViewById(R.id.etMa);
                final EditText etTen = dialogView.findViewById(R.id.etTen);
                final EditText etSoLuong = dialogView.findViewById(R.id.etSoLuong);
                final EditText etGia = dialogView.findViewById(R.id.etGia);

                Button btnLuu = dialogView.findViewById(R.id.btnLuu);
                Button btnHuy = dialogView.findViewById(R.id.btnHuy);

                etLoai.setText(product.getLoai());
                etMa.setText(product.getMa());
                etTen.setText(product.getTen());
                etSoLuong.setText("" + product.getSoLuong());
                etGia.setText("" + product.getDonGia());

                btnLuu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Product productSua = new Product(etLoai.getText().toString(),
                                product.getMa(),
                                etTen.getText().toString() ,
                                Integer.parseInt(etSoLuong.getText().toString()) ,
                                Integer.parseInt(etGia.getText().toString()) );

                        ProductDao productDao = new ProductDao(finalConvertView.getContext());
                        productDao.sua(productSua);
                        submitList(productDao.getAll());

                        alertDialog.dismiss();
                    }
                });

                btnHuy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
            }
        });

        return convertView;
    }

//    List<Product> list = new ArrayList<Product>();

    public void submitList(List<Product> p){
        listData.clear();
        listData.addAll(p);
        notifyDataSetChanged();
    }


    static class ViewHolder {
        TextView loai;
        TextView ma;
        TextView ten;
        TextView soLuong;
        TextView donGia;
        ImageView ivSua;
        ImageView ivXoa;
    }


}